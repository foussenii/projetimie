/** START Import Module Node */
const express = require('express'); // Module permettant de créé de route en NodeJs
const bodyParser = require('body-parser'); // Module permettant de parser les données envoyez 
/** END Import Module Node */

const app = express(); // Instantiation du module express

// creation d'un middelleware - type: application/x-www-form-urlencoded parser
let urlencodedParser = bodyParser.urlencoded({
    extended: false
})

// Création d'un jeu de données (Un Tableau d'Object) d'utilisateur
let users = [{
    "id": 1,
    "nom": "fousseni",
    "prenom": "coulibaly",
    "date_naissance": "22/11/1993",
    "lieu": "Paris"
}, {
    "id": 2,
    "nom": "Zoubida",
    "prenom": "Adibouz",
    "date_naissance": "11/02/191",
    "lieu": "Paris"
}]

/** 
 * @route /
 * @method GET
 */
app.get('/', function(req, res) {
    res.end("Coucou")
})

/** 
 * Affichage des utilisateurs
 * @route /users
 * @method GET
 */
app.get('/users', function(req, res) {
    res.writeHead(200, { 'Content-Type': 'application/json' }) // Préparation de la reponse
    res.end(JSON.stringify(users))
})

/** 
 * Affichage d'utilisateur
 * @route /users/:id
 * @method GET
 */
app.get('/users/:id', function(req, res) {
    res.writeHead(200, { 'Content-Type': 'application/json' }) // Préparation de la reponse
    res.end(JSON.stringify(users[req.params.id - 1])) // Affichage de l'utilisateur demander en JSON
})

/** 
 * Création d'utilisateur
 * @route /users
 * @method POST
 */
app.post('/users', urlencodedParser, function(req, res) {
    if (req.body.nom === undefined ||
        req.body.prenom === undefined ||
        req.body.date_naissance === undefined ||
        req.body.lieu === undefined) { // Si l'une des informations obligatoire pour la création n'a pas été envoyez 
        res.writeHead(402, { 'Content-Type': 'application/json' }) // Préparation de la reponse
        res.end(JSON.stringify({
            error: true,
            message: "Données manquant"
        }))
    } else {
        req.body.id = users.length + 1 // Création d'un id déterminer pas la taille du tableau d'utilisateur 
        users.push(req.body) // Ajout dans le tableau users
        res.writeHead(201, { 'Content-Type': 'application/json' }) // Préparation de la reponse        
        res.end(JSON.stringify(req.body))
    }
})

/** 
 * Mise à jour d'utilisateur
 * @route /users/:id
 * @method PUT
 */
app.put('/users/:id', urlencodedParser, function(req, res) {
    let newUser = users[req.params.id - 1]; // Récuperation de l'utilisateur demander - req.params.id contient l'id envoyez - (req.params.id - 1) car le tableau commence par 0

    if (req.body.nom !== undefined) // Si nom n'a pas été envoyez
        newUser.nom = req.body.nom // Mise à jour du nom
    if (req.body.prenom !== undefined) // Si prenom n'a pas été envoyez
        newUser.prenom = req.body.prenom // Mise à jour du prenom
    if (req.body.date_naissance !== undefined) // Si date_naissance n'a pas été envoyez
        newUser.date_naissance = req.body.date_naissance // Mise à jour du date_naissance
    if (req.body.lieu !== undefined) // Si lieu n'a pas été envoyez
        newUser.lieu = req.body.lieu // Mise à jour du lieu

    users[req.params.id - 1] = newUser; // Sauvegarde des nouvelle données de l'utilisateur 
    res.writeHead(200, { 'Content-Type': 'application/json' }) // Préparation de la reponse
    res.end(JSON.stringify(newUser))
})

/** 
 * Suppression de tout les utilisateurs
 * @route /users
 * @method DELETE
 */
app.delete('/users', function(req, res) {
    users = [] // Réinitialisation du tu tableau users
    res.writeHead(200, { 'Content-Type': 'application/json' }) // Préparation de la reponse
    res.end(JSON.stringify({
        error: false,
        message: "All user delete"
    }))
})

/** 
 * Suppression d'un utilisateur via sont id
 * @route /users/:id
 * @method DELETE
 */
app.delete('/users/:id', function(req, res) {
    delete users[req.params.id - 1] // Suppression de l'elements demander
    res.writeHead(200, { 'Content-Type': 'application/json' }) // Préparation de la reponse
    res.end(JSON.stringify({
        error: false,
        message: "User delete"
    }))
})

/** 
 * Ecoute du serveur sur le port 8080
 */
app.listen(8080, function() {
    console.log('Le serveur est run sur: http://localhost:8080/')
})